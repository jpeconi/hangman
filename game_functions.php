<?php
/**
 * Created by PhpStorm.
 * User: Jay
 * Date: 2016-09-18
 * Time: 1:28 PM
 */

// function to grab a random word from the array of words
function grabWord($words) {
    // Generate a random number to choose the word
    $randomNumber = rand(0,count($words) - 1);

    return $words[$randomNumber];
}

// Hide the characters of the word to be shown to the screen
function hideWord($secretWord) {
    $hiddenWord = "";
    for($x = 0; $x < strlen($secretWord); $x++) {
        $hiddenWord .= '*';
    }
    return $hiddenWord;
}

// Loop through the array of chars and push the index of the letter found
// into an index array
function uncoverLetters($letterGuessed)
{
    $isLetterFound = false;
    for($i = 0; $i < count($_SESSION['wordArray']); $i++) {
        if($_SESSION['wordArray'][$i] == $letterGuessed) {
            $_SESSION['hiddenWord'] = substr_replace($_SESSION['hiddenWord'],$letterGuessed,$i,1);
            $isLetterFound = true;
        }
    }
    return $isLetterFound;
}

// Function to generate the word and do the hiding and all that jazz
function startGame() {
    // Open the txt file to be read by the page
    $words = file("words.txt");
// Select a secret word
    $_SESSION['secretWord'] = trim(grabWord($words));
// Hide the word
    $_SESSION['hiddenWord'] = hideWord($_SESSION['secretWord']);
// Array to store all the letters from the word as a single character
    $_SESSION['wordArray'] = str_split($_SESSION['secretWord']);
// Default the number of wrong guesses to 0
    $_SESSION['numWrong'] = 0;
// Init the array of letters that have not been guessed to entire alphabet
    $_SESSION['lettersNotGuessed'] = range('a','z');
}

// Function to eliminate the letter from the array of letters
function guessedLetters() {
    $index = array_search($_GET['letter'],$_SESSION['lettersNotGuessed']);
    unset($_SESSION['lettersNotGuessed'][$index]);
    $_SESSION['lettersNotGuessed'] = array_values($_SESSION['lettersNotGuessed']);
}

// Function to check and see if the player has won
function checkWin() {
    $findMe = "*";
    if((strpos($_SESSION['hiddenWord'],$findMe) === false)) {
        return true;
    } else {
        return false;
    }
}