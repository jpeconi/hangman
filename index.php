<?php
/**
 * Created by PhpStorm.
 * User: jpeconi
 * Date: 9/12/2016
 * Time: 11:43 AM
 */
session_start();
include ('game_functions.php');
startGame();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hangman</title>
    <link href="main.css" rel="stylesheet">
</head>
<body>
    <div id="logoImage">
        <img id="logo" src="hangman assets/hangmanLogo.jpg">
        <div id="start">
            <a id="startText" href="game.php">Start Game</a>
        </div>
    </div>

</body>
</html>

