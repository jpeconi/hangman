<?php
/**
 * Created by PhpStorm.
 * User: Jay
 * Date: 2016-09-18
 * Time: 1:46 PM
 */
session_start();
include('game_functions.php');
$isGameOver = false;
$isWinner = false;
$isLoser = false;
// Increase the number of wrong guesses by 1 if the guessed letter is not found
// in the word. Also checks to see if the POST is null. (Had to do this to prevent
// the counter from going to 1 on the first time the page is loaded)
if(!uncoverLetters($_GET['letter']) && $_GET['letter'] != null) {
    $_SESSION['numWrong']++;
}

if($_GET['letter'] != null) {
    guessedLetters();
}

if($_SESSION['numWrong'] >= 7) {
    $isGameOver = true;
    $isLoser = true;
}

if(checkWin()) {
    $isGameOver = true;
    $isWinner = true;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hangman</title>
    <link href="main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Creepster|Ewert" rel="stylesheet">
</head>
<body>
    <div id="container">
        <div id="hangManPhoto">
            <?php
                echo "<img id='hangman' src='hangman assets/hangman".$_SESSION['numWrong'].".png-original'>";
            ?>
        </div>
        <div id="word">
            <?php
                if($isGameOver) {
                    if($isLoser) {
                        ?>
                        <div class="winner">
                            <h1>You Were Hanged!</h1>
                            <h2>The word was <?php echo $_SESSION['secretWord']; ?></h2>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="winner">
                            <h1>Great Work!</h1>
                            <h2>You guessed the word</h2>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <h1 id="hiddenWord"><?php echo $_SESSION['hiddenWord']; ?></h1>
                    <?php
                }
            ?>
        </div>
        <div id="letters">
            <?php
            if(!$isGameOver) {
                for ($i = 0; $i < count($_SESSION['lettersNotGuessed']); $i++) {
                    echo "<button type='button'><a href='game.php?letter=" . $_SESSION['lettersNotGuessed'][$i] . "'>" . $_SESSION['lettersNotGuessed'][$i] . "</a>";
                }
            } else {
                echo "<a id='prompt' href='index.php'>Play Again?</a>";
            }
            ?>
        </div>
    </div>
</body>
</html>
